#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "Forma.hpp"

class Block : public Forma{
   public:
      Block();
      ~Block();
      void setBlock(int pos_x, int pos_y);
      void alteraCampo(int pos_x, int pos_y,char **matriz);
};

#endif
