#ifndef CAMPO_HPP
#define CAMPO_HPP
#define LINHA 30
#define COLUNA 40
#include <string>

class Campo {
   private:
      char **matriz;
      int **vizinho;
   public:
      Campo();
      ~Campo();
      char **getMatriz();
      void iniciaMatriz();
      void imprimeCampo();
      void contaVizinho();
      void aplicaRegras();
};

#endif
