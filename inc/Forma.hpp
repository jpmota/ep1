#ifndef FORMA_HPP
#define FORMA_HPP

class Forma{
   protected:
      int pos_x;
      int pos_y;
   public:
      Forma();
      ~Forma();
      void setForma(int pos_x, int pos_y);
      virtual void alteraCampo(int pos_x, int pos_y,char **matriz);
};

#endif
