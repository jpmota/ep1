#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "Forma.hpp"

class Glider : public Forma{
   public:
      Glider();
      ~Glider();
      void setGlider(int pos_x, int pos_y);
      void alteraCampo(int pos_x, int pos_y,char **matriz);
};

#endif
