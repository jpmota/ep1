#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "Forma.hpp"

class Blinker : public Forma{
   public:
      Blinker();
      ~Blinker();
      Blinker(int pos_x, int pos_y);
      void alteraCampo(int pos_x, int pos_y,char **matriz);
};

#endif
