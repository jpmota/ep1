#include "Blinker.hpp"
#include "Forma.hpp"
Blinker::Blinker(){
   pos_x = 0;
   pos_y = 0;
};
Blinker::Blinker(int pos_x, int pos_y){
   this->pos_x = pos_x;
   this->pos_y = pos_y;
};
Blinker::~Blinker(){
};
void Blinker::alteraCampo(int pos_x, int pos_y,char **matriz){
   matriz[pos_x][pos_y] = 'O';
   matriz[pos_x+1][pos_y] = 'O';
   matriz[pos_x+2][pos_y] = 'O';
};
