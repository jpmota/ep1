#include "Glider.hpp"
#include "Forma.hpp"

Glider::Glider(){
   pos_x = 0;
   pos_y = 0;
};
Glider::~Glider(){
};
void Glider::setGlider(int pos_x, int pos_y){
   this->pos_x = pos_x;
   this->pos_y = pos_y;
};
void Glider::alteraCampo(int pos_x, int pos_y,char **matriz){
   matriz[pos_x][pos_y+1] = 'O';
   matriz[pos_x+1][pos_y+2] = 'O';
   matriz[pos_x+2][pos_y] = 'O';
   matriz[pos_x+2][pos_y+1] = 'O';
   matriz[pos_x+2][pos_y+2] = 'O';
};
