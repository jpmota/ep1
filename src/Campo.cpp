#include "Campo.hpp"
#include <iostream>
#include <stdio.h>

using namespace std;

Campo::Campo(){
};
Campo::~Campo(){
};
char **Campo::getMatriz(){
   return matriz;
}
void Campo::iniciaMatriz(){
   int i,j;
   matriz = new char*[LINHA];
   for(i = 0; i < LINHA; i++)
   {
     matriz[i] = new char[COLUNA];
   }
   vizinho = new int*[LINHA];
   for(i = 0; i < LINHA; i++)
   {
     vizinho[i] = new int[COLUNA];
   }
   for(i = 0;i < LINHA;i++)
   {
      for(j = 0; j < COLUNA;j++)
      {
         matriz[i][j] = ' ';
      }
   }
};
void Campo::imprimeCampo(){
   int i,j;
   for(i = 0;i < LINHA;i++)
   {
      printf("|");
      for(j = 0; j < COLUNA;j++)
      {
         printf("%c ",matriz[i][j]);
      }
      printf("| \n");
   }
};
void Campo::contaVizinho(){
   int i,j;
   for(i = 0;i < LINHA;i++)
   {
      for(j = 0; j < COLUNA;j++)
      {
         vizinho[i][j] = 0;
      }
   }
   for(i = 1;i < LINHA-1; i++)
   {
      for(j = 1;j < COLUNA-1;j++)
      {
         if(matriz[i-1][j-1] == 'O')
            vizinho[i][j]++;
         if(matriz[i-1][j] == 'O')
            vizinho[i][j]++;
         if(matriz[i-1][j+1] == 'O')
            vizinho[i][j]++;
         if(matriz[i][j-1] == 'O')
            vizinho[i][j]++;
         if(matriz[i][j+1] == 'O')
            vizinho[i][j]++;
         if(matriz[i+1][j-1] == 'O')
            vizinho[i][j]++;
         if(matriz[i+1][j] == 'O')
            vizinho[i][j]++;
         if(matriz[i+1][j+1] == 'O')
            vizinho[i][j]++;
      }
   }
};
void Campo::aplicaRegras(){
   int i,j;
   for(i = 0;i < LINHA; i++)
   {
      for(j = 0;j < COLUNA;j++)
      {
         if(vizinho[i][j] < 2)
            matriz[i][j] = ' ';
         if(vizinho[i][j] > 3)
            matriz[i][j] = ' ';
         if(vizinho[i][j] == 3)
            matriz[i][j] = 'O';
      }
   }
};
