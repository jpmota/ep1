#include "Block.hpp"
#include "Forma.hpp"

Block::Block(){
   pos_x = 0;
   pos_y = 0;
};
Block::~Block(){
};
void Block::setBlock(int pos_x, int pos_y){
   this->pos_x = pos_x;
   this->pos_y = pos_y;
};
void Block::alteraCampo(int pos_x, int pos_y,char **matriz){
   matriz[pos_x][pos_y] = 'O';
   matriz[pos_x][pos_y+1] = 'O';
   matriz[pos_x+1][pos_y] = 'O';
   matriz[pos_x+1][pos_y+1] = 'O';
};
