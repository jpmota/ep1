#include "Forma.hpp"
#include "Block.hpp"
#include "Blinker.hpp"
#include "Glider.hpp"
#include "Campo.hpp"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

void mostraMenu(){
   system("clear");
   cout << "      Conway's Game of Life" << endl;
   cout << "       >>>>>> Menu <<<<<<" << endl;
   cout << "0 - Sair" << endl;
   cout << "1 - Adicionar Block na posição desejada" << endl;
   cout << "2 - Adicionar Blinker na posição desejada" << endl;
   cout << "3 - Adicionar Glider na posição desejada" << endl;
   cout << "4 - Criar Gosper Glider Gun" << endl;
   cout << "Opção: ";
}
void criaGosper(Forma celula,Block block,Blinker blinker, Campo campo){
   block.alteraCampo(5,1,campo.getMatriz());
   block.alteraCampo(3,35,campo.getMatriz());
   blinker.alteraCampo(3,21,campo.getMatriz());
   blinker.alteraCampo(3,22,campo.getMatriz());
   blinker.alteraCampo(5,11,campo.getMatriz());
   blinker.alteraCampo(5,17,campo.getMatriz());
   celula.alteraCampo(1,25,campo.getMatriz());
   celula.alteraCampo(2,23,campo.getMatriz());
   celula.alteraCampo(2,25,campo.getMatriz());
   celula.alteraCampo(3,13,campo.getMatriz());
   celula.alteraCampo(3,14,campo.getMatriz());
   celula.alteraCampo(4,12,campo.getMatriz());
   celula.alteraCampo(4,16,campo.getMatriz());
   celula.alteraCampo(6,15,campo.getMatriz());
   celula.alteraCampo(6,18,campo.getMatriz());
   celula.alteraCampo(6,23,campo.getMatriz());
   celula.alteraCampo(6,25,campo.getMatriz());
   celula.alteraCampo(7,25,campo.getMatriz());
   celula.alteraCampo(8,12,campo.getMatriz());
   celula.alteraCampo(8,16,campo.getMatriz());
   celula.alteraCampo(9,13,campo.getMatriz());
   celula.alteraCampo(9,14,campo.getMatriz());
}
int main(){
    Campo campo;
    Forma celula;
    Blinker blinker;
    Block block;
    Glider glider;
    int x,y,i,geracoes,opcao;
    char continua;
    campo.iniciaMatriz();
    do{
         mostraMenu();
         cin >> opcao;
	 switch(opcao)
         {
            case 1:
   	      cout << "Posição X do Block: ";
	      cin >> x;
              cout << "Posição Y do Block: ";
              cin >> y;
	      block.alteraCampo(x,y,campo.getMatriz());
	      break;
	    case 2:
	      cout << "Posição X do Blinker: ";
	      cin >> x;
              cout << "Posição Y do Blinker: ";
              cin >> y;
	      blinker.alteraCampo(x,y,campo.getMatriz());
	      break;
	    case 3:
	      cout << "Posição X do Glider: ";
	      cin >> x;
              cout << "Posição Y do Glider: ";
              cin >> y;
              glider.alteraCampo(x,y,campo.getMatriz());
	      break;
	    case 4:
	      criaGosper(celula,block,blinker,campo);
         }
         if(opcao !=0)
         { 
	    cout << "Numero de gerações: ";
	    cin >> geracoes;
            system("clear");
	    for(i = 0; i <= geracoes; i++)
	    {
	       campo.imprimeCampo();
	       campo.contaVizinho();
	       campo.aplicaRegras();
               cout << "\n Aperte enter para ir para proxima geração ou p para voltar ao menu" << endl;
               continua = getchar();
               if (continua == 'p')
	          break;
	       system("clear");
	    }
         }
    }while(opcao != 0);
    return 0;
}

