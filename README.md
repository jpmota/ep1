Informações sobre o projeto: O campo do jogo é uma matriz 30x40, então a tela do terminal precisa estar de acordo.
É necessario apenas utilizar o Makefile.
O jogo não aplica as regras na borda.



				Regras
1.Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
2.Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.
3.Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.
4.Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração.
